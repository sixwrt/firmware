BR=buildroot
SDK=sdk
SDK_NAME=OpenWrt-SDK-ar71xx-for-$(shell uname -s)-$(shell uname -m)

all world: $(SDK)/feeds.conf
	$(MAKE) -C $(SDK) $@

clean menuconfig:
	$(MAKE) -C $(BR) $@

.PHONY: all clean world

$(SDK)/feeds.conf: $(SDK)/.config
	echo -n > $@
	echo "src-link packages $(BR)/feeds/packages" >> $@
	echo "src-link backports ../packages" >> $@
	echo "src-link sixwrt ../sixwrt/package" >> $@

$(SDK)/.config: $(BR)/bin/$(SDK_NAME).tar.bz2
	tar xfj $<
	mv $(SDK_NAME) $(SDK)

$(BR)/bin/$(SDK_FILE):$(BR)/.config
	$(MAKE) -C $(BR) world

$(BR)/.config: config
	cp $< $@
	$(MAKE) -C $(BR) .config
